## Description

This is a Bitcoin Random UI that allows you to generate random wallets and check the balance of each address, it notifies you when any address holds a balance greater than zero, allowing you to promptly download the details for further action.

## Installation

```bash
$ pnpm install
```

## Running the app

```bash
# development
$ pnpm dev

# production mode
$ pnpm start
```

