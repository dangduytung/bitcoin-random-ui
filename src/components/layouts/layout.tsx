import useLayout from '@/lib/hooks/use-layout';
import Header from './header';

export default function SiteLayout({
  children,
}: Readonly<React.PropsWithChildren<{}>>) {
  const { layout } = useLayout();
  return (
    <div className="flex min-h-screen flex-col bg-gray-100 transition-colors duration-150">
      <Header layout={layout} />
      {children}
    </div>
  );
}
export const getLayout = (page: React.ReactElement) => (
  <SiteLayout>{page}</SiteLayout>
);
