import LanguageSwitcher from '@/components/ui/language-switcher';
import Logo from '@/components/ui/logo';
import { useHeaderSearch } from '@/layouts/headers/header-search-atom';
import { useIsHomePage } from '@/lib/use-is-homepage';
import cn from 'classnames';
import { useTranslation } from 'next-i18next';
import { useMemo } from 'react';
import StaticMenu from './menu/static-menu';

const Header = ({ layout }: { layout?: string }) => {
  const { t } = useTranslation('common');
  const { show, hideHeaderSearch } = useHeaderSearch();
  const isHomePage = useIsHomePage();
  const isMultilangEnable =
    process.env.NEXT_PUBLIC_ENABLE_MULTI_LANG === 'true' &&
    !!process.env.NEXT_PUBLIC_AVAILABLE_LANGUAGES;

  const isFlattenHeader = useMemo(
    () => !show && isHomePage && layout !== 'modern',
    [show, isHomePage, layout]
  );
  return (
    <header
      className={cn(
        'site-header-with-search  top-0 z-50 h-14 w-full md:h-16 lg:h-22',
        {
          'lg:!h-auto': isFlattenHeader,
          fixed: isHomePage,
          sticky: !isHomePage,
        }
      )}
    >
      <div
        className={cn(
          ' flex h-14 w-full transform-gpu items-center justify-between border-b border-border-200 bg-light px-4 py-5 shadow-sm transition-transform duration-300 md:h-16 lg:h-22 lg:px-8',
          {
            'lg:absolute lg:border-0 lg:bg-transparent lg:shadow-none':
              isFlattenHeader,
          }
        )}
      >
        <div className="flex w-full items-center lg:w-auto">
          <Logo
            className={`${
              !isMultilangEnable ? 'mx-auto lg:mx-0' : 'ltr:ml-0 rtl:mr-0'
            }`}
          />

          {isMultilangEnable ? (
            <div className="ltr:ml-auto rtl:mr-auto lg:hidden">
              <LanguageSwitcher />
            </div>
          ) : (
            ''
          )}

        </div>

        <ul className="hidden shrink-0 items-center space-x-7 rtl:space-x-reverse lg:flex 2xl:space-x-10">
          <StaticMenu />
          {isMultilangEnable ? (
            <div className="ms-auto lg:me-5 xl:me-8 2xl:me-10 hidden flex-shrink-0 lg:block">
              <LanguageSwitcher />
            </div>
          ) : (
            ''
          )}
        </ul>
      </div>
    </header>
  );
};

export default Header;
