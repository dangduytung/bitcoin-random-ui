import { BTC, BalanceResponse } from '@/types';

type IProps = {
  btcs: BTC[] | undefined;
  balances: BalanceResponse | undefined;
};

const BTCList = ({ btcs, balances }: IProps) => {
  return (
    <div className="mb-6 overflow-x-auto rounded">
      {!!btcs?.length && (
        <table className="table-auto mx-auto">
          <thead>
            <tr>
              <th className="border px-4 py-2">Private Key (HEX)</th>
              <th className="border px-4 py-2">Bitcoin Addresses</th>
            </tr>
          </thead>
          <tbody>
            {btcs?.map((btc: BTC) => (
              <tr key={btc.privateKey}>
                <td className="border px-4 py-2">
                  <span>
                    <a
                      href={`https://privatekeyfinder.io/key/${btc.privateKey}`}
                      target="_blank"
                      rel="noopener noreferrer"
                      className="text-sm text-heading transition-colors hover:text-orange-500"
                    >
                      {btc.privateKey}
                    </a>
                  </span>
                </td>
                <td className="border px-4 py-2">
                  <div>
                    <span className="bg-gray-100 text-gray-800 text-xs font-medium me-2 px-2 py-0.5 rounded dark:bg-gray-700 dark:text-gray-400 border border-gray-500">
                      <small>C</small>
                    </span>
                    <span className="me-2">
                      <a
                        href={`https://www.blockchain.com/explorer/addresses/btc/${btc.addressC}`}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="text-sm text-heading transition-colors hover:text-orange-500"
                      >
                        {btc.addressC}
                      </a>
                    </span>
                    <span className="inline-block">
                      <span className="me-2 bg-purple-100 text-purple-800 text-xs font-medium px-2.5 py-0.5 rounded dark:bg-purple-900 dark:text-purple-300">
                        B {balances?.[btc.addressC!]?.final_balance ?? 0}
                      </span>
                      <span className="me-2 bg-rose-100 text-rose-800 text-xs font-medium px-2.5 py-0.5 rounded dark:bg-rose-900 dark:text-rose-300">
                        R {balances?.[btc.addressC!]?.total_received ?? 0}
                      </span>
                      <span className="me-2 bg-pink-100 text-pink-800 text-xs font-medium px-2.5 py-0.5 rounded dark:bg-pink-900 dark:text-pink-300">
                        T {balances?.[btc.addressC!]?.n_tx ?? 0}
                      </span>
                    </span>
                  </div>
                  <div>
                    <span className="bg-gray-100 text-gray-800 text-xs font-medium me-2 px-2 py-0.5 rounded dark:bg-gray-700 dark:text-gray-400 border border-gray-500">
                      <small>U</small>
                    </span>
                    <span>
                      <a
                        href={`https://www.blockchain.com/explorer/addresses/btc/${btc.addressU}`}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="text-sm text-heading transition-colors hover:text-orange-500"
                      >
                        {btc.addressU}
                      </a>
                    </span>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

export default BTCList;
