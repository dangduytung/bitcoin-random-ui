import { Image } from "@/components/ui/image";
import cn from "classnames";
import Link from "@/components/ui/link";
import { logoPlaceholder } from "@/lib/placeholders";
import { siteSettings } from "@/config/site";

// https://stackoverflow.com/questions/73386743/nextjs-image-was-preloaded-using-link-preload-but-not-used-within-a-few-seconds
const Logo: React.FC<React.AnchorHTMLAttributes<{}>> = ({
  className,
  ...props
}) => {
  return (
    <Link href="/" className={cn("inline-flex", className)} {...props}>
      <span className="relative h-10 w-32 overflow-hidden md:w-40">
        <Image
          src={siteSettings?.logo?.url ?? logoPlaceholder}
          placeholder="blur"
          blurDataURL={siteSettings?.logo?.url ?? logoPlaceholder}
          alt={siteSettings?.siteTitle || "Bitcoin Logo"}
          fill
          sizes="(max-width: 768px) 100vw"
          loading="eager"
          className="object-contain"
        />
      </span>
    </Link>
  );
};

export default Logo;
