import React from 'react';

interface CircleWithNumberProps {
  number: string;
}

const CircleWithNumber: React.FC<CircleWithNumberProps> = ({ number }) => {
  const circleRadius = 50;

  return (
    <svg width={2 * circleRadius} height={2 * circleRadius}>
      <circle cx={circleRadius} cy={circleRadius} r={circleRadius} fill="lightblue" />
      <text x="50%" y="50%" textAnchor="middle" dominantBaseline="middle" fontSize="14" fill="black">
        {number}
      </text>
    </svg>
  );
};

export default CircleWithNumber;
