import React from 'react';

interface CircleWithNumberProps {
  number: string;
}

const CircleWithNumber1: React.FC<CircleWithNumberProps> = ({ number }) => {
  const circleRadius = 18; // You can adjust the radius as needed

  const circleStyle: React.CSSProperties = {
    width: `${2 * circleRadius}px`,
    height: `${2 * circleRadius}px`,
    borderRadius: '50%',
    backgroundColor: 'rgba(0, 0, 0, 0)', // Transparent background
    border: '2px solid rgb(248 113 113)', // Red border
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    padding: '1px',
  };

  const textStyle: React.CSSProperties = {
    fontSize: '20px',
    color: 'black',
    position: 'absolute',
  };

  return (
    <div style={circleStyle}>
      <span style={textStyle}>{number}</span>
    </div>
  );
};

export default CircleWithNumber1;
