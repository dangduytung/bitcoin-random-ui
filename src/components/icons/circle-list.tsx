// components/CircleList.tsx
import React, { CSSProperties } from 'react';
import CircleWithNumber1 from '../ui/icons/circle-with-number-1';

interface CircleListProps {
  numbers: string[];
  containerStyle?: CSSProperties;
}

const CircleList: React.FC<CircleListProps> = ({ numbers, containerStyle }) => {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'left',
        alignItems: 'left',
        ...containerStyle,
      }}
    >
      {numbers.map((number, index) => (
        <>
          <CircleWithNumber1 key={number} number={number} />
          &nbsp;
        </>
      ))}
    </div>
  );
};

export default CircleList;
