import { Routes } from '@/config/routes';

export const siteSettings = {
  name: 'Bitcoin',
  siteTitle: 'Bitcoin Keys',
  description: '',
  logo: {
    url: '/logo_bg_white.png',
    alt: 'Bitcoin',
    width: 128,
    height: 40,
  },
  defaultLanguage: 'en',
  currencyCode: 'USD',
  footer: {
    copyright: {
      name: 'BITCOIN',
      href: 'https://blockchain.com',
    },
    address: '',
    email: '',
    phone: '',
    menus: [
      {
        title: 'text-explore',
        links: [
          {
            name: 'Random',
            href: Routes.random,
          },
        ],
      },
    ],
  },
};
