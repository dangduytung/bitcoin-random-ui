export function getRandomNumberInRange(
  startKey: string,
  stopKey: string
): string {
  const start = BigInt(`0x${startKey}`);
  const stop = BigInt(`0x${stopKey}`);

  if (startKey >= stopKey) {
    return startKey;
  }

  const range = stop - start;

  const bytesNeeded = Math.ceil(range.toString(16).length / 2); // Calculate the number of bytes needed to represent the range

  let randomNumber: bigint;
  do {
    // Generate random bytes
    const randomBytes = new Uint8Array(bytesNeeded);
    for (let i = 0; i < bytesNeeded; i++) {
      randomBytes[i] = Math.floor(Math.random() * 256); // Random byte value between 0 and 255
    }
    // Convert random bytes to a BigInt
    randomNumber = BigInt(
      `0x${randomBytes.reduce(
        (acc, val) => acc + val.toString(16).padStart(2, '0'),
        ''
      )}`
    );
  } while (randomNumber >= range);

  // Add the start value to the random number
  randomNumber += start;

  // Convert the random number to a hexadecimal string
  let hexString = randomNumber.toString(16);

  // Pad the string with zeros to ensure a length of 64 characters
  while (hexString.length < 64) {
    hexString = '0' + hexString;
  }

  return hexString;
}

export function generateHexRange(bits: number): { from: string; to: string } {
  const PRIVATE_KEY_LENGTH = 64;
  const from = BigInt(2 ** (bits - 1))
    .toString(16)
    .padStart(PRIVATE_KEY_LENGTH, '0');
  const to = (BigInt(2 ** bits) - BigInt(1))
    .toString(16)
    .padStart(PRIVATE_KEY_LENGTH, '0');
  return { from, to };
}
