export function formatNumber(number: number): string {
  if (isNaN(number)) {
    return '';
  }
  const formattedAmount = new Intl.NumberFormat('vi-VN').format(number);
  return formattedAmount;
}

export function formatCurrencyVND(amount: number): string {
  if (isNaN(amount)) {
    return '';
  }
  const currencyFormatter = new Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND',
    minimumFractionDigits: 0,
  });

  const formattedCurrency = currencyFormatter.format(amount);

  // Replace the currency symbol '₫' with 'VND'
  const formattedCurrencyWithVND = formattedCurrency.replace('₫', 'VND');

  return formattedCurrencyWithVND;
}
