import type { NextPage } from "next";
import type { ReactElement, ReactNode } from "react";

export type NextPageWithLayout<P = {}> = NextPage<P> & {
  authenticationRequired?: boolean;
  getLayout?: (page: ReactElement) => ReactNode;
};

export type LayoutProps = {
  readonly children: ReactNode;
};

export interface HomePageProps {
  variables: {
  };
  layout: string;
}

export interface QueryOptions {
  language: string;
  page?: number;
  limit?: number;
}

export interface PaginatorInfo<T> {
  current_page: number;
  data: T[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  links: any[];
  next_page_url: string | null;
  path: string;
  per_page: number;
  prev_page_url: string | null;
  to: number;
  total: number;
}

export interface BTC {
  mnemonic: string;
  privateKey: string;
  wifC: string;
  publicKeyC: string;
  addressC?: string;
  wifU: string;
  publicKeyU: string;
  addressU?: string;
}

export interface BalanceResponse {
  [key: string]: {
    final_balance: number;
    n_tx: number;
    total_received: number;
  };
}
