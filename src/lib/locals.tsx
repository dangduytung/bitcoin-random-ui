import { USFlag } from '@/components/icons/flags/USFlag';
import { VNFlag } from '@/components/icons/flags/VNFlag';
import { useRouter } from 'next/router';

import { USFlagRound } from '@/components/icons/flags/USFlagRound';
import { VNFlagRound } from '@/components/icons/flags/VNFlagRound';

const localeRTLList = ['ar', 'he'];
export function useIsRTL() {
  const { locale } = useRouter();
  if (locale && localeRTLList.includes(locale)) {
    return { isRTL: true, alignLeft: 'right', alignRight: 'left' };
  }
  return { isRTL: false, alignLeft: 'left', alignRight: 'right' };
}

export const languageMenu = [
  {
    id: 'vi',
    name: 'Vietnam',
    value: 'vi',
    icon: <VNFlag width="20px" height="15px" />,
    iconMobile: <VNFlagRound />,
  },
  {
    id: 'en',
    name: 'English',
    value: 'en',
    icon: <USFlag width="20px" height="15px" />,
    iconMobile: <USFlagRound />,
  },
];
