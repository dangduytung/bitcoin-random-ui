export const TOKEN = 'token';
export const AUTH_TOKEN_KEY = 'auth_token';
export const AUTH_PERMISSIONS = 'auth_permissions';
export const LIMIT = 10;

export const BTC_GENERATE_NUMBER = 400;

export const RTL_LANGUAGES: ReadonlyArray<string> = ["ar", "he"];

export function getDirection(language: string | undefined) {
  if (!language) return "ltr";
  return RTL_LANGUAGES.includes(language) ? "rtl" : "ltr";
}
