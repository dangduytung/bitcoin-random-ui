import { getLayout } from '@/components/layouts/layout';
import BTCList from '@/components/random/btc-list';
import { BTC, BalanceResponse, NextPageWithLayout } from '@/types';
import { generateHexRange } from '@/utils/bitcoin.util';
import axios from 'axios';
import { useRouter } from 'next/router';
import { ParsedUrlQuery } from 'querystring';
import { useEffect, useState } from 'react';

interface QueryParams extends ParsedUrlQuery {
  startKey?: string;
  stopKey?: string;
}

const RandomPage: NextPageWithLayout<{}> = () => {
  const router = useRouter();

  const REFRESH_INTERVAL = 3000; // Define the refresh interval in milliseconds

  const [btcs, setBtcs] = useState<BTC[]>([]);
  const [balances, setBalances] = useState<{ [key: string]: any }>({});
  const [totalBalance, setTotalBalance] = useState(0);
  const [totalReceived, setTotalReceived] = useState(0);
  const [totalTransactions, setTotalTransactions] = useState(0);

  const [errorMessage, setErrorMessage] = useState('');
  const [countRequests, setCountRequests] = useState(0);

  const [bits, setBits] = useState('');
  const [startKey, setStartKey] = useState('');
  const [stopKey, setStopKey] = useState('');

  const [intervalId, setIntervalId] = useState<NodeJS.Timeout | null>(null);

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      searchInRange();
      updateURL();
    }
  };

  const searchInRange = () => {
    refreshBtcs(startKey, stopKey);
  };

  const updateURL = () => {
    const query: QueryParams = {};
    if (startKey !== '') {
      query.startKey = startKey;
    }
    if (stopKey !== '') {
      query.stopKey = stopKey;
    }

    router.push({
      pathname: router.pathname,
      query,
    });
  };

  // Function to initiate download of balances text file
  const downloadText = (text: BlobPart, fileName: string) => {
    const element = document.createElement('a');
    const file = new Blob([text], { type: 'text/plain' });
    element.href = URL.createObjectURL(file);
    element.download = fileName || 'data.txt';
    document.body.appendChild(element); // Required for Firefox
    element.click();
  };

  const refreshBtcs = async (startKey: string = '', stopKey: string = '') => {
    // console.log('refreshBtcs', startKey, stopKey);
    try {
      // Call API endpoint to fetch updated btcs data
      const response = await axios.get('/api/generate-btcs', {
        params: {
          startKey,
          stopKey,
        },
      });
      setBtcs(response.data);
    } catch (error) {
      console.error('Error refreshing btcs:', error);
    }
  };

  // Function to start the interval
  function startInterval() {
    if (!intervalId) {
      const newIntervalId = setInterval(() => {
        refreshBtcs(startKey, stopKey);
      }, REFRESH_INTERVAL);
      setIntervalId(newIntervalId);
    }
  }

  // Function to stop the interval
  function stopInterval() {
    if (intervalId) {
      clearInterval(intervalId);
      setIntervalId(null);
    }
  }

  useEffect(() => {
    const fetchBalances = async () => {
      if (btcs.length === 0) {
        return;
      }

      setCountRequests((prevCount) => prevCount + 1);

      try {
        setErrorMessage(''); // Clear any previous error messages

        const addresses = btcs.map((btc: BTC) => btc.addressC);
        const response = await axios.get(
          `https://blockchain.info/balance?active=${addresses.join('|')}`
        );
        const balanceData: BalanceResponse = response.data;
        const newBalances: { [key: string]: any } = {};

        let balancesText = '';
        let totalBalance = 0;
        let totalReceived = 0;
        let totalTransactions = 0;

        for (const [address, data] of Object.entries(balanceData)) {
          // Check if balance is greater than 0
          if (data.final_balance > 0 || data.total_received > 0) {
            const balance = data.final_balance / 1e8; // Convert satoshis to BTC
            const received = data.total_received / 1e8; // Convert satoshis to BTC
            console.log(
              `Address ${address} has a balance of ${balance} BTC, received ${received} BTC.`
            );
            balancesText += `Address ${address} has a balance of ${balance} BTC, received ${received} BTC.\n`;
            alert(
              `Address ${address} has a balance of ${balance} BTC, received ${received} BTC.`
            );
          }

          // Save balances has > 0 as text to a file
          if (balancesText !== '') {
            const dataSaved = balancesText + '\n\n' + JSON.stringify(btcs);
            downloadText(dataSaved, 'btcs.txt');
          }

          newBalances[address] = data;

          totalBalance += data.final_balance / 1e8;
          totalReceived += data.total_received / 1e8;
          totalTransactions += data.n_tx;
        }

        setBalances(newBalances);
        setTotalBalance(totalBalance);
        setTotalReceived(totalReceived);
        setTotalTransactions(totalTransactions);
      } catch (error) {
        // Handle error here
        console.error('Error fetching balances:', error);
        setErrorMessage(
          `An error occurred while fetching balances. Please try again later`
        );
      }
    };

    fetchBalances();
  }, [btcs]);

  useEffect(() => {
    if (router.isReady) {
      const { bits, startKey: start, stopKey: stop } = router.query;
      let startKeyInit = '';
      let stopKeyInit = '';
      if (bits) {
        const { from, to } = generateHexRange(parseInt(bits as string, 10));
        startKeyInit = from;
        stopKeyInit = to;
        setBits(bits as string);
      } else if (typeof start === 'string' && typeof stop === 'string') {
        startKeyInit = start;
        stopKeyInit = stop;
      }
      setStartKey(startKeyInit);
      setStopKey(stopKeyInit);

      refreshBtcs(startKeyInit, stopKeyInit);
    }
  }, [router.isReady, router.query]);

  return (
    <div className="container mx-auto">
      <div className="text-center">
        <h1 className="text-2xl font-bold my-4 mx-auto">
          Random Bitcoin Private Keys
        </h1>
        <div>
          <h2 className="text-xl my-4">
            <span className="me-2">Total Balance</span>
            <span className="me-2 bg-purple-100 text-purple-800 text-sm font-medium px-2.5 py-0.5 rounded dark:bg-purple-900 dark:text-purple-300">
              B {totalBalance}
            </span>
            <span className="me-2 bg-rose-100 text-rose-800 text-sm font-medium px-2.5 py-0.5 rounded dark:bg-rose-900 dark:text-rose-300">
              R {totalReceived}
            </span>
            <span className="me-2 bg-pink-100 text-pink-800 text-sm font-medium px-2.5 py-0.5 rounded dark:bg-pink-900 dark:text-pink-300">
              T {totalTransactions}
            </span>
          </h2>
          <div className="w-full">
            <button
              onClick={() => refreshBtcs(startKey, stopKey)}
              disabled={!!errorMessage || intervalId !== null}
              style={
                !!errorMessage || intervalId !== null
                  ? { opacity: 0.5, cursor: 'not-allowed' }
                  : {}
              }
              className="me-2 bg-transparent hover:bg-violet-600 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"
            >
              Refresh
            </button>
            <button
              onClick={() => {
                startInterval();
              }}
              disabled={!!errorMessage || intervalId !== null}
              style={
                !!errorMessage || intervalId !== null
                  ? { opacity: 0.5, cursor: 'not-allowed' }
                  : {}
              }
              className="me-2 bg-transparent hover:bg-red-500 text-orange-700 font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded"
            >
              Start Interval
            </button>
            <button
              onClick={() => {
                stopInterval();
              }}
              disabled={intervalId === null}
              style={
                !!errorMessage || intervalId === null
                  ? { opacity: 0.5, cursor: 'not-allowed' }
                  : {}
              }
              className="bg-transparent hover:bg-red-500 text-orange-700 font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded"
            >
              Stop Interval
            </button>
          </div>
        </div>
      </div>
      <div className="md:w-[64rem] mx-auto pl-2 mt-2">
        <div className="text-xs">
          <a
            className="text-blue-500"
            href="https://privatekeys.pw/puzzles/bitcoin-puzzle-tx"
            target="_blank"
            rel="noopener noreferrer"
          >
            Bitcoin Puzzle
          </a>
          {[66, 67, 68, 69, 70, 71].map((b) =>
            [70].includes(b) ? (
              <span className="ml-2 text-gray-300" key={b}>
                {b}
              </span>
            ) : (
              <a
                href={`?bits=${b}`}
                key={b}
                className={`ml-2 hover:underline ${
                  parseInt(bits) === b
                    ? 'text-red-600 hover:text-red-700 font-bold'
                    : 'text-orange-500'
                }`}
              >
                {b}
              </a>
            )
          )}
        </div>
      </div>
      <div className="md:w-[64rem] mx-auto mt-2">
        <div className="grid gap-4 mb-4 md:grid-cols-1">
          <div>
            <input
              type="text"
              id="startKey"
              value={startKey}
              onChange={(e) => setStartKey(e.target.value)}
              onKeyDown={handleKeyDown}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Start Key Range (hex 64 length)"
              required
            />
          </div>
          <div>
            <input
              type="text"
              id="stopKey"
              value={stopKey}
              onChange={(e) => setStopKey(e.target.value)}
              onKeyDown={handleKeyDown}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Stop Key Range (hex 64 length)"
              required
            />
          </div>
        </div>
      </div>
      <div className="md:w-[64rem] mx-auto mt-2">
        <div className="text-gray-500 text-sm">
          <span className="mx-1">
            Count:{' '}
            <span className="text-pink-600 font-bold">{countRequests}</span>
          </span>
          {' | '}
          <span className="text-sm">{btcs.length} random addresses</span>
          {errorMessage && (
            <>
              {' | '}
              <span className="text-xs text-red-700">({errorMessage})</span>
            </>
          )}
        </div>
      </div>
      <div className="mt-4">
        <BTCList btcs={btcs} balances={balances} />
      </div>
    </div>
  );
};

RandomPage.getLayout = getLayout;

export default RandomPage;
