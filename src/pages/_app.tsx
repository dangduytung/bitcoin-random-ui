import '@/assets/css/main.css';
import QueryProvider from '@/data/client/query-provider';
import { getDirection } from '@/lib/constants';
import { NextPageWithLayout } from '@/types';
import { SessionProvider } from 'next-auth/react';
import { appWithTranslation } from 'next-i18next';
import type { AppProps } from 'next/app';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import 'react-toastify/dist/ReactToastify.css';
const ToastContainer = dynamic(
  () => import('react-toastify').then((module) => module.ToastContainer),
  { ssr: false }
);

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

function CustomApp({
  Component,
  pageProps: {
    //@ts-ignore
    session,
    ...pageProps
  },
}: AppPropsWithLayout) {
  // Use the layout defined at the page level, if available
  const getLayout = Component.getLayout ?? ((page) => page);
  const { locale } = useRouter();
  const dir = getDirection(locale);

  const router = useRouter();

  useEffect(() => {
    if (router.asPath === '/') {
      router.push('/random');
    }
  }, [router]);

  return (
    <div dir={dir}>
      <SessionProvider session={session}>
        <QueryProvider pageProps={pageProps}>
          <>
            {getLayout(<Component {...pageProps} />)}
            <ToastContainer autoClose={2000} theme="colored" />
          </>
        </QueryProvider>
      </SessionProvider>
    </div>
  );
}

export default appWithTranslation(CustomApp);
