import { generateRandomBtcs } from '@/data/bitcoin';
import { BTC_GENERATE_NUMBER } from '@/lib/constants';
import { NextApiRequest, NextApiResponse } from 'next';
export { getServerSideProps } from '@/data/bitcoin.ssr';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    const { number, startKey, stopKey } = req.query;
    const btcs = await generateRandomBtcs(
      Number(number) || BTC_GENERATE_NUMBER,
      startKey as string,
      stopKey as string
    );
    res.status(200).json(btcs);
  } catch (error) {
    console.error('Error refreshing BTC data:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}
