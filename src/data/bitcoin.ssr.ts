import { GetServerSideProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { generateRandomBtcs } from './bitcoin';

export const getServerSideProps: GetServerSideProps = async ({ locale }) => {
  const btcs = await generateRandomBtcs();
  return {
    props: {
      ...(await serverSideTranslations(locale!, ['common', 'banner'])),
      btcs,
    },
  };
};
