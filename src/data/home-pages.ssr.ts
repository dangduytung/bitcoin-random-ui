import type {
  // CategoryQueryOptions,
  HomePageProps,
} from '@/types';
import type { GetStaticPaths, GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import invariant from 'tiny-invariant';

type ParsedQueryParams = {
  pages: string[];
};

// This function gets called at build time
export const getStaticPaths: GetStaticPaths<ParsedQueryParams> = async ({
  locales,
}) => {
  invariant(locales, 'locales is not defined');
  return {
    paths: locales?.map((locale) => ({ params: { pages: [] }, locale })),
    fallback: 'blocking',
  };
};

export const getStaticProps: GetStaticProps<
  HomePageProps,
  ParsedQueryParams
> = async ({ locale, params }) => {
  return {
    props: {
      variables: {},
      layout: 'default',
      ...(await serverSideTranslations(locale!, ['common', 'banner'])),
    },
    revalidate: 120,
  };
};
