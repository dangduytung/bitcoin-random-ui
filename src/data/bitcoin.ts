import { BTC_GENERATE_NUMBER } from '@/lib/constants';
import { BTC } from '@/types';
import { getRandomNumberInRange } from '@/utils/bitcoin.util';
import * as bip39 from 'bip39';
import * as bitcoin from 'bitcoinjs-lib';
import { ECPairFactory } from 'ecpair';
import * as tinysecp from 'tiny-secp256k1';

const MAIN_NET = true;
const NETWORK = MAIN_NET ? bitcoin.networks.bitcoin : bitcoin.networks.testnet;
const ECPair = ECPairFactory(tinysecp);

export async function getBtc(privateKey: string): Promise<BTC> {
  // console.log(`privateKey: ${privateKey}`);
  const mnemonic = bip39.entropyToMnemonic(privateKey);
  const buffer = Buffer.from(privateKey, 'hex');

  /* Compressed */
  const keyPairC = ECPair.fromPrivateKey(buffer, {
    network: NETWORK,
  });
  const wifC = keyPairC.toWIF();
  const publicKeyC = keyPairC.publicKey.toString('hex');
  const addressC = bitcoin.payments.p2pkh({
    pubkey: keyPairC.publicKey,
    network: NETWORK,
  }).address;

  /* Uncompressed */
  const keyPairU = ECPair.fromPrivateKey(buffer, {
    compressed: false,
    network: NETWORK,
  });
  const wifU = keyPairU.toWIF();
  const publicKeyU = keyPairU.publicKey.toString('hex');
  const addressU = bitcoin.payments.p2pkh({
    pubkey: keyPairU.publicKey,
    network: NETWORK,
  }).address;

  const btc: BTC = {
    mnemonic,
    privateKey,
    wifC,
    publicKeyC,
    addressC,
    wifU,
    publicKeyU,
    addressU,
  };
  // console.log(`btc: ${JSON.stringify(btc)}`);
  return btc;
}

export function generateRandomPrivateKey(
  startKey: string = '',
  stopKey: string = ''
): string {
  const characters = '0123456789abcdef';

  let privateKey = '';
  if (startKey.length === 64 && stopKey.length === 64) {
    privateKey = getRandomNumberInRange(startKey, stopKey);
  } else {
    for (let i = 0; i < 64; i++) {
      const randomIndex = Math.floor(Math.random() * characters.length);
      privateKey += characters.charAt(randomIndex);
    }
  }

  return privateKey;
}

export async function generateRandomBtcs(
  number: number = BTC_GENERATE_NUMBER,
  startKey: string = '',
  stopKey: string = ''
): Promise<BTC[]> {
  // console.log(`number: ${number}, startKey: ${startKey}, stopKey: ${stopKey}`);

  const privateKeys = Array.from({ length: number }, () =>
    generateRandomPrivateKey(startKey, stopKey)
  );

  // Fetch BTC values asynchronously
  const btcs = await Promise.all(privateKeys.map(getBtc));
  return btcs;
}
